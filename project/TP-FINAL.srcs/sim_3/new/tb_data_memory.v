`timescale 1ns / 1ps

module tb_data_memory;

    parameter NB_DATA       = 32;
    parameter N_REG         =  32;
    parameter NB_INDEX_REG  =  $clog2(N_REG);

    //inputs
    reg i_clk;
    reg i_reset;
    reg i_data_write_enb;
    reg i_data_read_enb;
    reg [NB_DATA-1:0] i_data_addr;
    reg [NB_DATA-1:0] i_write_data;
    reg [NB_DATA-1:0] i_add_pc_plus4_shift_left2;
    reg [NB_INDEX_REG-1:0] i_reg_write_dest;

    //outputs         
    wire [NB_DATA-1:0] o_data_addr;
    wire [NB_DATA-1:0] o_read_data;
    wire [NB_DATA-1:0] o_add_pc_plus4_shift_left2;
    wire [NB_INDEX_REG-1:0] o_reg_write_dest;
 
 MEM memoria
    ( 
	    .i_clk                          (i_clk),
        .i_reset                        (i_reset),
        .i_data_write_enb               (i_data_write_enb),
        .i_data_read_enb                (i_data_read_enb),
        .i_data_addr                    (i_data_addr),
        .i_write_data                   (i_write_data),
        .i_add_pc_plus4_shift_left2     (i_add_pc_plus4_shift_left2),
        .i_reg_write_dest               (i_reg_write_dest),
        .o_data_addr                    (o_data_addr),
        .o_read_data                    (o_read_data),
        .o_add_pc_plus4_shift_left2     (o_add_pc_plus4_shift_left2),
        .o_reg_write_dest               (o_reg_write_dest)
    );
    
initial begin
    i_clk = 0;
    i_reset = 1;
	i_data_write_enb = 0;
    i_data_read_enb = 0;
	i_data_addr = {32{1'b0}};
	i_write_data = {32{1'b0}};
    i_add_pc_plus4_shift_left2 = {32{1'b0}};
    i_reg_write_dest = {5{1'b0}};
	#20
	i_reset = 0;
	i_data_write_enb = 1;
	#40
	i_reset = 1;
	i_data_write_enb = 0;
	#20
	i_reset = 0;
    i_data_read_enb = 1;
end
    
    always begin //clock de la placa 100Mhz-> (#5)
            #10 i_clk=~i_clk; //50Mhz
    end  
    
    always@(posedge i_clk)
		begin
			if(i_reset)
                i_data_addr = {32{1'b0}}; 
			else
				i_data_addr = i_data_addr + 1'b1;
		end  
    
endmodule    