`timescale 1ns / 1ps

module tb_ID;
    //inputs
    reg clk;
    reg reset;
    reg instruction_write_enb;
    reg [31:0] instruction_addr;
    reg [31:0] instruction_data;
    reg reg_write_dest;
    
    wire [31:0] instruction;
    wire [31:0] o_rs;
    wire [31:0] o_rt;
    wire [31:0] o_sign_extend;

    //instancia IF            
    IF test_if(
       .i_clk(clk),	
       .i_reset(reset),
       .i_instruction_write_enb(instruction_write_enb),
       .i_instruction_addr(instruction_addr),
       .i_instruction_data(instruction_data),
           
       .o_IF(instruction)
    );
    //instancia ID
    ID test_id(
        .i_clk(clk),	
        .i_reset(reset),
        .i_instruction(instruction),       
        .o_rs_data(o_rs),
        .o_rt_data(o_rt),
        .o_sign_extend(o_sign_extend)
    );
    
    //logic
    initial begin
        clk = 0;
        reset = 1;
        reg_write_dest = 0;
        instruction_write_enb = 0;
        instruction_addr = {32{1'b0}};
        instruction_data = {32{1'b0}};
        #60
        reset = 0;
        reg_write_dest = 1;
    end
    
    always begin //clock de la placa 100Mhz-> (#5)
            #10 clk=~clk; //50Mhz
    end
    
endmodule