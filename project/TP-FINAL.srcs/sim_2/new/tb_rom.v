`timescale 1ns / 1ps

module tb_ROM;

    //inputs
    reg i_clk;
    reg reset;
    reg instruction_write_enb;
    reg [31:0] instruction_addr;
    reg [31:0] instruction_data;
    reg [31:0] PC_count;    
    //outputs         
    wire [31 : 0] out_mem_data;
    
    //ROM
	ROM
	#(
        .ROM_WIDTH      (32),
        .ROM_ADDR_BITS  (32),
        .FILE           ("rom_test.mem") 
    )
    rom
    ( 
	    .i_clk              (i_clk),
        .i_write_enable     (instruction_write_enb),
        .i_write_addr       (instruction_addr),
        .i_data             (instruction_data),
        .i_read_addr        (PC_count),
        .o_data             (out_mem_data)
    );
    
initial begin
    i_clk = 0;
    reset = 1;
	instruction_write_enb = 0;
	instruction_addr = {32{1'b0}};
	instruction_data = {32{1'b0}};
	#20
	reset = 0;
	instruction_write_enb = 1;
	#40
	reset = 1;
	instruction_write_enb = 0;
	#20
	reset = 0;
end
    
    always begin //clock de la placa 100Mhz-> (#5)
            #10 i_clk=~i_clk; //50Mhz
    end  
    
    always@(posedge i_clk)
		begin
			if(reset == 0) 
				PC_count = PC_count + 1'b1; 
			else
				PC_count = {32{1'b1}};
		end  
    
endmodule    