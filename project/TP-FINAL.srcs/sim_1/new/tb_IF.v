`timescale 1ns / 1ps

module tb_IF;
    //inputs
    reg clk; 
    reg reset;
    reg instruction_write_enb;
    reg [31:0] instruction_addr;
    reg [31:0] instruction_data;
    //outputs         
    wire [31:0] out_IF;
       
    IF test_if(
       .i_clk(clk),	
       .i_reset(reset),
       .i_instruction_write_enb(instruction_write_enb),
       .i_instruction_addr(instruction_addr),
       .i_instruction_data(instruction_data),
           
       .o_IF(out_IF)
    );
    
    initial begin
        clk = 0;
        reset = 1;
        instruction_write_enb = 0;
        instruction_addr = {32{1'b0}};
        instruction_data = {32{1'b0}};
        #20
        reset = 0;
    end
    
    always begin //clock de la placa 100Mhz-> (#5)
            #10 clk=~clk; //50Mhz
    end
        
endmodule
