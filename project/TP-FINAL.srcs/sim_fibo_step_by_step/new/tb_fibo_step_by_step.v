`timescale 1ns / 1ps

module tb_fibo_step_by_step;    
    
    localparam ROM_WIDTH = 32;
    localparam MAX_SIZE = 64;
    
    //inputs
    reg clk;
    reg reset;
    reg mips_enable;
    reg extern_halt;
    reg instruction_write_enb;
    reg [31:0] instruction_addr;
    reg [31:0] instruction_data;
    reg [ROM_WIDTH-1:0] rom [MAX_SIZE - 1   :0];
        
    //instancia MIPS            
    MIPS_toplevel#(
        //.ROM_FILE("sim_fibonacci.mem")
    ) mips(        
        .i_clk(clk),
        .i_reset(reset),
        .i_mips_enable(mips_enable),
        .i_extern_halt(extern_halt),
	    .instruction_write_enb(instruction_write_enb),
        .instruction_addr(instruction_addr),
        .instruction_data(instruction_data)               
    );
    
    //logic
    initial begin
        $readmemb("sim_fibonacci.mem", rom, 0);
        clk = 0;
        reset = 1;
        mips_enable = 1;
        extern_halt = 0;
        instruction_write_enb = 0;        
        instruction_addr = -1;
        instruction_data = rom[0];
 
        reset = 0;
        extern_halt = 1;
        instruction_write_enb = 1;

        #260 
        extern_halt = 0;
        instruction_write_enb = 0;
        reset = 1;
        #20
        reset = 0;
        #200
        mips_enable = 0;
        #200
        mips_enable = 1;
        #200
        mips_enable = 0;
        #200
        mips_enable = 1;
        #200
        mips_enable = 0;
        #200
        mips_enable = 1;

    end
    
    always begin //clock de la placa 100Mhz-> (#5)
            #10 clk=~clk; //50Mhz
    end
    
    always@(posedge clk)
    begin
      instruction_addr = instruction_addr + 1'b1;
    end
    
    always@(posedge clk)
    begin
      instruction_data = rom[instruction_addr];
    end
    
endmodule