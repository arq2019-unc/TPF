`timescale 1ns / 1ps

module IF#(
    parameter NB_DATA = 32,
    parameter ROM_FILE = ""

 )(	
	input wire i_clk,	
	input wire i_reset,
	input wire i_mips_enable,
	input wire i_instruction_write_enb,
	input wire [NB_DATA-1 : 0] i_instruction_addr,
	input wire [NB_DATA-1 : 0] i_instruction_data,
	input wire [NB_DATA-1:0] i_add_pc_plus4_shift_left2,
	
	//control
	input wire i_pc_src,
	
	//hazard
	input wire i_stall,
	
	input wire i_jump,
	input wire [NB_DATA-1:0] i_jump_addr,
	
	input wire i_extern_halt,
	
	//bus de salida de la etapa IF
	output [NB_DATA-1:0] o_IF,
	output [NB_DATA-1:0] o_pc_plus4	
    );
    
	//PC Program Counter
	reg [NB_DATA-1:0] PC_count;
	reg [NB_DATA-1:0] pc_plus4;
	
	wire [NB_DATA-1 : 0] out_mem_data;
	wire                        halt_detection;
    assign halt_detection = (i_extern_halt)? 1'b1 : (out_mem_data == 32'hFFFFFFFF) ? 1'b1 : 1'b0;

	
	always@(posedge i_clk)
    begin
        if(i_reset) 
            PC_count <= {32{1'b0}};
        else if (i_mips_enable && (i_stall || halt_detection))
            PC_count <= PC_count;    
        else if (i_mips_enable && (i_jump))
            PC_count <= i_jump_addr;                    
        else if (i_mips_enable && (i_pc_src))
            PC_count <= i_add_pc_plus4_shift_left2;
        else if (i_mips_enable)
            PC_count <= PC_count + 1'b1; 
    end
    
    always@(posedge i_clk)
    begin
        if(i_reset) 
            pc_plus4 <= {32{1'b0}} + 1'b1;
        else if (i_mips_enable && (i_stall || halt_detection))
            pc_plus4 <= pc_plus4;                        
        else if (i_mips_enable && (i_pc_src | i_jump))
            pc_plus4 <= {32{1'b0}};
        else if (i_mips_enable)
            pc_plus4 <= PC_count + 1'b1;
    end
		
	assign o_pc_plus4 = pc_plus4;
	
	//ROM
	ROM
	#(
        .ROM_WIDTH      (32),
        .ROM_ADDR_BITS  (32),
        .FILE           (ROM_FILE)         
    )
    rom
    ( 
	    .i_clk              (i_clk),
        .i_write_enable     (i_instruction_write_enb),
        .i_write_addr       (i_instruction_addr),
        .i_data             (i_instruction_data),
        .i_read_addr        (PC_count),
        .o_data             (out_mem_data)
    );
    
    //instruction reg
    reg [NB_DATA-1:0] instruction;
	always@(posedge i_clk)
		begin
		      if(i_reset) 
				  instruction <= {32{1'b0}};
			  else if (i_mips_enable && halt_detection)
			     instruction <= {32{1'b0}};
			  else if (i_mips_enable && i_stall)
			     instruction <= instruction;
			  else if (i_mips_enable && (i_pc_src | i_jump))
			     instruction <= {32{1'b0}};
			  else if (i_mips_enable)
			     instruction <= out_mem_data;
		end
 
	assign o_IF = instruction;	 

endmodule
