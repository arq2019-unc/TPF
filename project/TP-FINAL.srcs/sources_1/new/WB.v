`timescale 1ns / 1ps

module WB#(
    parameter NB_DATA       =  32,
    parameter N_REG         =  32,
    parameter NB_INDEX_REG  =  $clog2(N_REG)
)(    
    input wire [NB_DATA-1:0]            i_mem_read_data,
    input wire [NB_DATA-1:0]            i_alu_result,        
    input wire                          i_mem_to_reg,
    input wire [NB_INDEX_REG-1:0]       i_reg_write_dest,
    input wire                          i_reg_write_enb,
   
    output wire [NB_DATA-1:0] o_reg_write_data,
    output wire [NB_INDEX_REG-1:0] o_reg_write_addr,
    output wire                    o_reg_write_enb
    );
    
    assign o_reg_write_data = (i_mem_to_reg)?  i_mem_read_data : i_alu_result; 
    assign o_reg_write_addr = i_reg_write_dest;
    assign o_reg_write_enb = i_reg_write_enb;
    
endmodule
