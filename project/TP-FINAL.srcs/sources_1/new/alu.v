`timescale 1ns / 100ps
 
module ALU
#
(
    parameter NB_SHAMT         =              5,
    parameter NB_FUNC          =              6,
    parameter NB_DATA          =             32
)(
    input   wire    [NB_DATA  - 1:0]      i_rs_data,
    input   wire    [NB_DATA  - 1:0]      i_rt_data,
    input   wire    [NB_SHAMT - 1:0]      i_shamt,
    input   wire    [NB_FUNC  - 1:0]      i_func,
    output  reg     [NB_DATA  - 1:0]      o_result,
    output  wire                          o_zero      
);

localparam      SLL        = 6'b000000;  // Left  Shift i_dataRT shamt  
localparam      SRL        = 6'b000010;  // Right Shift i_dataRT shamt (insertando ceros) L de logic
localparam      SRA        = 6'b000011;  // Right Shift i_dataRT shamt (Aritmetico, conservando el sig)
localparam      SLLV       = 6'b000100;  // Left  Shift i_dataRT << i_dataRS
localparam      SRLV       = 6'b000110;  // Right Shift i_dataRT >> i_dataRS (insertando ceros) L de logic
localparam      SRAV       = 6'b000111;  // Right Shift i_dataRT >> i_dataRS (conservando signo)
localparam      ADD        = 6'b110001;
localparam      ADDU       = 6'b100001; // Add unsigned
localparam      SUBU       = 6'b100011;  // rs - rt (signed obvio)
localparam      AND        = 6'b100100;
localparam      OR         = 6'b100101;
localparam      XOR        = 6'b100110;
localparam      NOR        = 6'b100111;
localparam      SLT        = 6'b101010;
localparam      SHIFTLUI   = 6'b101011;

assign o_zero = ( o_result == 'd0) ? 1'b1 : 1'b0 ;
    
always @(*)
    case (i_func)
        /* Shift */
        SLL  : o_result    = i_rt_data  <<  i_shamt;
        SRL  : o_result    = i_rt_data  >>  i_shamt;
        SRA  : o_result    = $signed(i_rt_data) >>>  i_shamt;
        SLLV : o_result    = i_rt_data  <<  i_rs_data;
        SRLV : o_result    = i_rt_data  >>  i_rs_data;
        SRAV : o_result    = $signed(i_rt_data) >>>  i_rs_data;
        SHIFTLUI: o_result = {i_rt_data[15:0],{16{1'b0}}} ;
        /* aritmetica basica */
        ADD  : o_result    = $signed (i_rs_data) + $signed (i_rt_data);
        ADDU : o_result    = i_rs_data + i_rt_data;
        SUBU : o_result    = $signed (i_rs_data) - $signed (i_rt_data);
        /* Logic  */
        AND  : o_result    =  i_rs_data & i_rt_data;
        OR   : o_result    =  i_rs_data | i_rt_data;
        XOR  : o_result    =  i_rs_data ^ i_rt_data;
        NOR  : o_result    = ~(i_rs_data | i_rt_data);
        /**/
        SLT  : o_result    = ($signed (i_rs_data) < $signed (i_rt_data)) ? 'd1 : 'd0;
        
        default: o_result   = {NB_DATA{1'b1}};
    endcase
endmodule
