`timescale 1ns / 1ps

module alu_control_unit#(
    parameter NB_INDEX_FUNC_FIELD  =  6,
    parameter NB_INDEX_ALU_OP  =  3
 )
 (	
    input wire [NB_INDEX_FUNC_FIELD-1 : 0] i_func_field,
    input wire [NB_INDEX_ALU_OP-1 : 0] i_alu_op,

    output reg [NB_INDEX_FUNC_FIELD-1:0] o_alu_control
);

/* crt alu codes */

localparam EXE_ALUOP_ADD      = 3'b000;
localparam EXE_ALUOP_SUB      = 3'b001;
localparam EXE_ALUOP_FUNC     = 3'b010;
localparam EXE_ALUOP_AND      = 3'b011;
localparam EXE_ALUOP_OR       = 3'b100;
localparam EXE_ALUOP_XOR      = 3'b101;
localparam EXE_ALUOP_SHIFTLUI = 3'b110;
localparam EXE_ALUOP_SLTI     = 3'b111;


/* ALU CODES */

localparam SUBU     = 6'b100011;
localparam AND      = 6'b100100;
localparam OR       = 6'b100101;
localparam XOR      = 6'b100110;
localparam NOR      = 6'b100111;
localparam SLT      = 6'b101010;
localparam ADD      = 6'b110001;
localparam SHIFTLUI = 6'b101011;

always @(*)
begin
    case (i_alu_op)
        EXE_ALUOP_ADD      :  o_alu_control =             ADD;
        EXE_ALUOP_SUB      :  o_alu_control =            SUBU;
        EXE_ALUOP_FUNC     :  o_alu_control =    i_func_field;
        EXE_ALUOP_AND      :  o_alu_control =             AND;
        EXE_ALUOP_OR       :  o_alu_control =              OR;
        EXE_ALUOP_XOR      :  o_alu_control =             XOR;
        EXE_ALUOP_SHIFTLUI :  o_alu_control =        SHIFTLUI;
        EXE_ALUOP_SLTI     :  o_alu_control =             SLT;
        default            :  o_alu_control = {NB_INDEX_FUNC_FIELD{1'b1}};
    endcase;
end
    
endmodule
