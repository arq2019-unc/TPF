`timescale 1ns / 1ps

module MEM#(
    parameter NB_DATA       = 32,
    parameter N_REG         =  32,
    parameter NB_INDEX_REG  =  $clog2(N_REG),    
    parameter _NB_ENABLE = NB_DATA / 8
 )(	
	input wire i_clk,	
	input wire i_reset,
	input wire i_mips_enable,
	input wire i_data_write_enb,
    input wire i_data_read_enb,              // La lectura de una direccion no valida puede causar problemas
	input wire [NB_DATA-1:0] i_data_addr,
	input wire [NB_DATA-1:0] i_write_data,
    //input wire [NB_DATA-1:0] i_add_pc_plus4_shift_left2,
    input wire [NB_INDEX_REG-1:0] i_reg_write_dest,
    //input wire i_zero,                                                      //no haria mas falta aca
    //input wire i_mem_branch,                                                //pasar de MEM->EX
    input wire [1:0] i_mem_byte_enb,
    input wire [1:0] i_mem_ext_sig,
    input wire i_write_back_ctl,
    input wire i_reg_write_enb,

	output wire [NB_DATA-1:0] o_data_addr,
    output wire [NB_DATA-1:0] o_read_data,
    //output wire [NB_DATA-1:0] o_add_pc_plus4_shift_left2,
    output wire [NB_INDEX_REG-1:0] o_reg_write_dest, 
    //output wire o_branch_taken,                                               //pasar de MEM->EX      
    output wire o_write_back_ctl,
    output wire o_reg_write_enb
    );
    
    wire [NB_DATA - 1 : 0 ] real_addr;

    reg [NB_DATA-1:0] data_addr;
    reg [NB_DATA-1:0] mem_data;
    reg [NB_INDEX_REG-1:0] reg_write_dest;    
    reg write_back_ctl;
    reg reg_write_enb;
    reg [_NB_ENABLE-1:0] byte_enb;

    assign real_addr  = {2'b00,i_data_addr[NB_DATA - 1 : 2]};
    
    
    //assign o_add_pc_plus4_shift_left2 = i_add_pc_plus4_shift_left2;
    assign o_reg_write_dest = reg_write_dest;
    assign o_data_addr = data_addr;
    //assign o_branch_taken = (i_zero & i_mem_branch) ? 1'b1 : 1'b0;                  //pasar de MEM->EX
    assign o_write_back_ctl = write_back_ctl;
    assign o_reg_write_enb = reg_write_enb;
    
    always @(posedge i_clk)
    begin
        if(i_reset)
            reg_write_enb <= 0;
        else if (i_mips_enable)
            reg_write_enb <= i_reg_write_enb;  
    end
    
    always @(posedge i_clk)
    begin
        if(i_reset)
            write_back_ctl <= 0;
        else if (i_mips_enable)
            write_back_ctl <= i_write_back_ctl;  
    end
    
    always @(posedge i_clk)
    begin
        if(i_reset)
            reg_write_dest <= 0;
        else if (i_mips_enable)
            reg_write_dest <= i_reg_write_dest;  
    end
    
    always @(posedge i_clk)
    begin
        if(i_reset)
            data_addr <= 0;
        else if (i_mips_enable)
            data_addr <= i_data_addr;  
    end
    
    //write logic
    always @(*)
    begin
        if( i_data_write_enb == 1'b1 && i_data_read_enb == 1'b0
            && i_mem_byte_enb ==  2'b01)
                byte_enb = 4'b0001;
        else if( i_data_write_enb == 1'b1 && i_data_read_enb == 1'b0 
            && i_mem_byte_enb ==  2'b00)
                byte_enb = 4'b0011;
        else
            byte_enb = 4'b1111;
    end
    
    //read logic
    // Codificacion para seleccion lectura de byte
    reg [3:0] sel_byte;
    reg [3:0] sel_half;
    reg [3:0] rd_byte_enable;
    always @(*)
    begin
        case (i_data_addr [1:0])
            2'b00 : sel_byte = 4'b0001;
            2'b01 : sel_byte = 4'b0010;
            2'b10 : sel_byte = 4'b0100;
            2'b11 : sel_byte = 4'b1000;
        endcase
    end
    
    // Codificacion para seleccion lectura de media palabra
    always @(*)
    begin
        case (i_data_addr [1:0])
            2'b00   : sel_half = 4'b0011;
            2'b10   : sel_half = 4'b1100;
            default : sel_half = 4'b1111;
        endcase
    end
    
    // Seleccion de lectura byte/half/word
    always @(*)
    begin
        case (i_mem_byte_enb)
            2'b01 : rd_byte_enable = sel_byte;
            2'b00 : rd_byte_enable = sel_half;
            2'b11 : rd_byte_enable =  4'b1111;
            default         : rd_byte_enable =  4'b0000;
        endcase
    end
    //-------usa el dato leido
    // Shifteo de datos para la cargar alineado en el banco de registro el dato
    localparam MASK_LOWER = 32'h0000ffff;
    localparam FULL_WORD_ENB    = 4'b1111;
    localparam HALF_UPPER_ENB   = 4'b1100;
    localparam HALF_LOWER_ENB   = 4'b0011;
    localparam BYTE_ZERO_ENB    = 4'b0001;
    localparam BYTE_ONE_ENB     = 4'b0010;
    localparam BYTE_TWO_ENB     = 4'b0100;
    localparam BYTE_THREE_ENB   = 4'b1000;
    wire [NB_DATA - 1 : 0 ] mem_out_data; //data read from memory
    reg [NB_DATA - 1 : 0 ]  read_data;
    always @ (*)
    begin
        case(rd_byte_enable)
            HALF_LOWER_ENB  : read_data    = {16'h0000  , mem_out_data[0  +: 16]};
            HALF_UPPER_ENB  : read_data    = {16'h0000  , mem_out_data[16 +: 16]};
            BYTE_ZERO_ENB   : read_data    = {24'h000000, mem_out_data[0  +: 8]};
            BYTE_ONE_ENB    : read_data    = {24'h000000, mem_out_data[8  +: 8]};
            BYTE_TWO_ENB    : read_data    = {24'h000000, mem_out_data[16 +: 8]};
            BYTE_THREE_ENB  : read_data    = {24'h000000, mem_out_data[24 +: 8]};
            FULL_WORD_ENB   : read_data    = mem_out_data;
            default         : read_data    = 32'hAA_AA_AA_AA;
        endcase
    end
    
    // Ajuste de extencion de signo para lectura
    reg [NB_DATA - 1 : 0 ]  read_data_signed;
    always @(*)
    begin
        case(i_mem_ext_sig)
            2'b01    : read_data_signed = {{24{read_data[7]}} , read_data[0 +: 8]};
            2'b10    : read_data_signed = {{16{read_data[15]}}, read_data[0 +: 16]};
            default             : read_data_signed = read_data;
        endcase
    end
    
    
    always @(posedge i_clk)
    begin
        if (i_reset)
            mem_data   <= 'd0;
        else if (i_mips_enable)
            mem_data   <= read_data_signed;
    end
    assign o_read_data   = mem_data;
    
    //instancia memory_data
    memory_data
	#(
        .DATA_FILE           ("data_test.mem") 
    )
    banco_memoria
    (
        .i_clk               (i_clk),
        .i_data_write_enb    (i_mips_enable & i_data_write_enb),
        .i_data_read_enb     (i_data_read_enb),
        .i_byte_enb          (byte_enb),   
        .i_data_addr         (real_addr),
        .i_write_data        (i_write_data),
            
        .o_read_data         (mem_out_data)
    );
    

endmodule
