`timescale 1ns / 1ps

module main_control_unit#(
       parameter NB_INDEX_INSTRUCTION_OP  =  6,       
       parameter NB_INDEX_ALU_OP  =  3
)(
    input wire [NB_INDEX_INSTRUCTION_OP-1:0] i_instruction_op,
    input wire [NB_INDEX_INSTRUCTION_OP-1:0] i_instruction_func,
    
    //ID
    output wire o_id_reg_dest,
    output wire o_id_reg_write,
    //EX  
    output wire [NB_INDEX_ALU_OP-1:0] o_ex_alu_op,
    output wire o_ex_alu_src,
    output wire [2 : 0] o_exe_jump_type,
    //MEM
    output wire o_mem_write_enabled,
    output wire o_mem_read_enabled,
    output wire [1 : 0] o_mem_branch,       //es PCSrc 
    output wire [1 : 0] o_mem_byte_enb,
    output wire [1 : 0] o_mem_ext_sig,
    //WB
    output wire o_write_back_ctl   //es memToReg en grafico pag 265
    );

    // localparam
    localparam NB_MEM_BYTE_ENB    = 4;
    localparam FUNC_JR            = 6'b001000;   
    localparam FUNC_JALR          = 6'b001001;

    // I-types
    localparam OPCODE_I_MASK        = 6'b111000;
    
    // Loads
    localparam OPCODE_I_LOAD_TYPE   = 6'b100000; // 16-21 (doc drive)
    localparam OPCODE_I_LOAD_LB     = 3'b000;
    localparam OPCODE_I_LOAD_LH     = 3'b001;
    localparam OPCODE_I_LOAD_LW     = 3'b011;
    localparam OPCODE_I_LOAD_LBU    = 3'b100;
    localparam OPCODE_I_LOAD_LHU    = 3'b101;
    localparam OPCODE_I_LOAD_LWU    = 3'b111;
    
    // Stores
    localparam OPCODE_I_STORE_TYPE   = 6'b101000; // 16-21 (doc drive)
    localparam OPCODE_I_STORE_SB     = 3'b000;
    localparam OPCODE_I_STORE_SH     = 3'b001;
    localparam OPCODE_I_STORE_SW     = 3'b011;
    
    // Literal
    localparam OPCODE_I_LITERAL_LOGIC       = 4'b0011;
    localparam I_TYPE_ARITMETIC_AND         = 2'b00;
    localparam I_TYPE_ARITMETIC_OR          = 2'b01;
    localparam I_TYPE_ARITMETIC_XOR         = 2'b10;
    localparam I_TYPE_ARITMETIC_LUI         = 6'b11;
    localparam OPCODE_I_TYPE_ARITMETIC_ADD   = 6'b001000;
    localparam OPCODE_I_TYPE_ARITMETIC_SLTI  = 6'b001010;
    
    //
    localparam OPCODE_I_TYPE_BRANCH_EQ      = 6'b000100;
    localparam OPCODE_I_TYPE_BRANCH_NE      = 6'b000101;
    localparam OPCODE_I_TYPE_J      = 6'b000010;
    localparam OPCODE_I_TYPE_JAL    = 6'b000011;

    // control Signals
    reg                                 exe_alu_src;
    reg  [2 : 0]      exe_alu_op;
    reg  [1 : 0]      exe_is_branch; // Asig
    wire [2 : 0]      exe_jump_type; // Asig
    reg                                 exe_reg_dest;  // Asig
    reg               mem_read_enb;    
    reg               mem_write_enb;
    reg  [1 : 0]      mem_byte_enb;
    reg  [1 : 0]      mem_ext_sig;
    reg               wrb_mem_to_reg;
    reg               wrb_write_enb_reg;

    // instruction type Signals
    wire                                R_type;
    wire                                I_type_load;
    wire                                I_type_store;
    wire                                I_type_aritmetic_logic;
    wire                                I_type_artimetic_logic_add;
    wire                                I_type_aritmetic_slti;
    wire                                I_type_branch_eq;
    wire                                I_type_branch_ne;
    wire                                I_type_j;
    wire                                I_type_jal;
    wire                                J_type_jr;
    wire                                J_type_jalr;

    assign  R_type                      = (i_instruction_op == 6'b000000) && (i_instruction_func != FUNC_JR) && (i_instruction_func != FUNC_JALR);
    assign  I_type_load                 = ((i_instruction_op &  OPCODE_I_MASK) == OPCODE_I_LOAD_TYPE);
    assign  I_type_store                = ((i_instruction_op &  OPCODE_I_MASK) == OPCODE_I_STORE_TYPE);
    assign  I_type_aritmetic_logic      = (i_instruction_op [NB_INDEX_INSTRUCTION_OP -1 -: 4] == OPCODE_I_LITERAL_LOGIC);
    assign  I_type_artimetic_logic_add  = (i_instruction_op ==  OPCODE_I_TYPE_ARITMETIC_ADD);
    assign  I_type_aritmetic_slti       = (i_instruction_op ==  OPCODE_I_TYPE_ARITMETIC_SLTI);

    assign  I_type_branch_eq            = (i_instruction_op == OPCODE_I_TYPE_BRANCH_EQ);
    assign  I_type_branch_ne            = (i_instruction_op == OPCODE_I_TYPE_BRANCH_NE);

    assign I_type_j                     = (i_instruction_op == OPCODE_I_TYPE_J);
    assign I_type_jal                   = (i_instruction_op == OPCODE_I_TYPE_JAL);

    assign J_type_jr                    = (i_instruction_op == 6'b000000) && (i_instruction_func == FUNC_JR);
    assign J_type_jalr                  = (i_instruction_op == 6'b000000) && (i_instruction_func == FUNC_JALR);

    assign exe_jump_type                = (I_type_j)     ? 3'b001
                                        :(I_type_jal)  ? 3'b010
                                        :(J_type_jr)   ? 3'b011
                                        :(J_type_jalr) ? 3'b100
                                        : 3'b000;

    assign o_id_reg_dest = exe_reg_dest;
    assign o_id_reg_write = wrb_write_enb_reg;
    assign o_ex_alu_op = exe_alu_op;
    assign o_ex_alu_src = exe_alu_src;
    assign o_exe_jump_type = exe_jump_type;
    assign o_mem_write_enabled = mem_write_enb;
    assign o_mem_read_enabled = mem_read_enb;
    assign o_mem_byte_enb = mem_byte_enb;
    assign o_mem_ext_sig = mem_ext_sig;
    assign o_mem_branch = exe_is_branch;
    assign o_write_back_ctl = wrb_mem_to_reg;
    
    //EX
    always @ (*)
    begin
        exe_alu_src   = 0;
        exe_alu_op    = 0;
        exe_reg_dest  = 0;
        exe_is_branch = 2'b00;
        if (R_type)
        begin
            exe_alu_src  = 1'b0; 
            exe_alu_op   = 3'b010; 
            exe_reg_dest = 1'b1;
        end
        else if(I_type_load | I_type_store)
        begin
            exe_alu_src  = 1'b1;
            exe_alu_op   = 3'b000; 
            exe_reg_dest = 1'b0;
        end
        else if(I_type_aritmetic_logic)
        begin
            exe_alu_src  = 1'b1; 
            exe_reg_dest = 1'b0;
            
            case (i_instruction_op [1:0])
            I_TYPE_ARITMETIC_AND: 
                exe_alu_op   = 3'b011;
            I_TYPE_ARITMETIC_OR:
                exe_alu_op   = 3'b100;
            I_TYPE_ARITMETIC_XOR:
                exe_alu_op   = 3'b101;
            I_TYPE_ARITMETIC_LUI:
                exe_alu_op   = 3'b110;
            endcase
        end
        else if(I_type_artimetic_logic_add)
        begin
            exe_alu_src  = 1'b1; 
            exe_reg_dest = 1'b0;
            exe_alu_op   = 3'b000;
        end
        else if(I_type_aritmetic_slti)
        begin
            exe_alu_src  = 1'b1; 
            exe_reg_dest = 1'b0;
            exe_alu_op   = 3'b111;
        end
        else if(I_type_branch_eq)
        begin
            exe_alu_src  = 1'b0; 
            exe_reg_dest = 1'b0;
            exe_alu_op   = 3'b101;
            exe_is_branch = 2'b01;
        end
        else if(I_type_branch_ne)
        begin
            exe_alu_src  = 1'b0; 
            exe_reg_dest = 1'b0;
            exe_alu_op   = 3'b101;
            exe_is_branch = 2'b10;
        end
    end
    
    //mem
    always @ (*)
    begin
        mem_read_enb  = 1'b0;
        mem_write_enb = 1'b0;  
        mem_byte_enb  = 2'b00;      
        mem_ext_sig  = 2'b00;
        if (R_type | I_type_aritmetic_logic | I_type_artimetic_logic_add 
                   | I_type_aritmetic_slti  | I_type_branch_ne | I_type_branch_eq
                   | I_type_j | I_type_jal  | J_type_jr | J_type_jalr)
        begin
            mem_byte_enb = 2'b11;
            mem_read_enb = 1'b0;
            mem_write_enb = 1'b0; 
            mem_ext_sig = 2'b00;        
        end
        else if(I_type_load)
        begin
            mem_write_enb = 1'b0;
            mem_read_enb  = 1'b1;
            
            // Indican si el load es signado o no, y la cantidad de bytes a leer
            case (i_instruction_op [2:0])
                OPCODE_I_LOAD_LB:
                begin
                    mem_byte_enb  = 2'b01;
                    mem_ext_sig   = 2'b01;
                end
                OPCODE_I_LOAD_LH:
                begin  
                    mem_byte_enb  = 2'b00;
                    mem_ext_sig   = 2'b10;
                end 
                OPCODE_I_LOAD_LW:
                begin
                    mem_byte_enb  = 2'b11;
                    mem_ext_sig   = 2'b00;                  
                end 
                OPCODE_I_LOAD_LBU:
                begin
                    mem_byte_enb  = 2'b01;
                    mem_ext_sig   = 2'b00;
                end 
                OPCODE_I_LOAD_LHU:
                begin
                    mem_byte_enb  = 2'b00;
                    mem_ext_sig   = 2'b00;
                end 
                OPCODE_I_LOAD_LWU:
                begin
                    mem_byte_enb  = 2'b11;
                    mem_ext_sig   = 2'b00;
                end  
                
            endcase
        end
        else if(I_type_store)
        begin
            mem_read_enb = 1'b0;
            mem_write_enb = 1'b1;
            
            // Indican si el load es signado o no, y la cantidad de bytes a leer
            case (i_instruction_op [2:0])
                OPCODE_I_STORE_SB:
                begin
                    mem_byte_enb  = 2'b01;
                    mem_ext_sig   = 2'b00;
                end
                OPCODE_I_STORE_SH:
                begin
                    mem_byte_enb  = 2'b00;
                    mem_ext_sig   = 2'b00;
                end 
                OPCODE_I_STORE_SW:
                begin
                    mem_byte_enb  = 2'b11;
                    mem_ext_sig   = 2'b00;                    
                end             
            endcase
        end
    end
    
    
    // write back
    always @ (*)
    begin
        wrb_mem_to_reg    = 1'b0;
        wrb_write_enb_reg = 1'b0;
        if (R_type | I_type_aritmetic_logic 
                   | I_type_artimetic_logic_add | I_type_aritmetic_slti
                   | I_type_jal  | J_type_jalr)
        begin
            wrb_mem_to_reg    = 1'b0;
            wrb_write_enb_reg = 1'b1; 
        end
        else if(I_type_load)
        begin
            wrb_mem_to_reg    = 1'b1;
            wrb_write_enb_reg = 1'b1; 
        end
        else if(I_type_store | I_type_branch_ne | I_type_branch_eq
                | I_type_j   | J_type_jr)
        begin
            wrb_mem_to_reg    = 1'b0;
            wrb_write_enb_reg = 1'b0; 
        end
    end
endmodule