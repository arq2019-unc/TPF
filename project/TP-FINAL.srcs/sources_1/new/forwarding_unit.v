`timescale 1ns / 1ps


module forwarding_unit#(
    parameter N_REG         =  32,
    parameter NB_INDEX_REG  =  $clog2(N_REG),
    parameter NB_FORWARD_SEL = 2

)(
    input [NB_INDEX_REG-1:0] i_rs_addr,
    input [NB_INDEX_REG-1:0] i_rt_addr,  
    input [NB_INDEX_REG-1:0] i_ex_reg_dest,
    input [NB_INDEX_REG-1:0] i_mem_reg_dest,
    input                    i_ex_wb_ctl,
    input                    i_mem_wb_ctl,
    
    output [NB_FORWARD_SEL-1:0] o_fwd_rs_select,
    output [NB_FORWARD_SEL-1:0] o_fwd_rt_select
    );
    
    assign o_fwd_rs_select = ((i_rs_addr == i_ex_reg_dest ) & i_ex_wb_ctl) 
                        ? 2'b01
                        :((i_rs_addr == i_mem_reg_dest ) & i_mem_wb_ctl) 
                            ? 2'b10
                            : 2'b00;

    assign o_fwd_rt_select = ((i_rt_addr == i_ex_reg_dest ) & i_ex_wb_ctl) 
                        ? 2'b01
                        :((i_rt_addr == i_mem_reg_dest ) & i_mem_wb_ctl) 
                            ? 2'b10
                            : 2'b00;   
endmodule
