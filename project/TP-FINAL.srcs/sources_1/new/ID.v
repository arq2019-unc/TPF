`timescale 1ns / 1ps

module ID#(
    parameter NB_DATA       =  32,
    parameter N_REG         =  32,
    parameter NB_INDEX_REG  =  $clog2(N_REG),
    parameter NB_INDEX_ALU_OP  =  3

)(
    input wire i_clk,	
	input wire i_reset,
	input wire i_mips_enable,
    input wire [NB_DATA-1:0] i_instruction,
    input wire [NB_INDEX_REG-1:0] i_reg_write_dest,     //viene del write back
    input wire [NB_DATA-1:0] i_write_reg_data,         //viene del write back
    input wire [NB_DATA-1:0] i_pc_plus4,  
    input wire               i_reg_write_enb,
    input wire               i_nop,  
        
    //datapath    
    output wire [NB_DATA-1:0] o_rs_data,
    output wire [NB_DATA-1:0] o_rt_data,
    output wire [NB_INDEX_REG-1:0] o_read_rs_addr,
    output wire [NB_INDEX_REG-1:0] o_read_rt_addr,
    output wire [NB_INDEX_REG-1:0] o_write_reg_addr, 
    output wire [NB_DATA-1:0] o_sign_extend,
    output wire [NB_DATA-1:0] o_pc_plus4,    
    //control
    output wire o_reg_write_enb,                 //es el reg_write_enb q va hasta el final
    output wire [2:0]   o_alu_op,
    output wire o_alu_src,
    output wire [2:0] o_exe_jump_type,
    output wire [25:0] o_addr_index,
    output wire o_mem_write_enabled,
    output wire o_mem_read_enabled, 
    output wire [1:0] o_mem_branch,
    output wire [1:0] o_mem_byte_enb,
    output wire [1:0] o_mem_ext_sig,
    output wire o_write_back_ctl,
    output wire o_reg_write_dest                    //va hasta el mux de EX
    );
    
    //local wires and regs 
    wire [NB_INDEX_REG-1:0] read_rs_addr; 
    wire [NB_INDEX_REG-1:0] read_rt_addr; 
    wire [NB_INDEX_REG-1:0] write_reg_addr; 

    reg  [NB_DATA-1 : 0]    rs_data;
    reg  [NB_DATA-1 : 0]    rt_data;
    wire [NB_DATA-1 : 0]    register_file_rs_data;
    wire [NB_DATA-1 : 0]    register_file_rt_data;
    reg  [NB_DATA-1 : 0]    sign_extend;
       
    reg write_back_ctl;   
    reg [NB_DATA-1:0] pc_plus4;
    
    reg                             reg_write_dest_d;
    reg                             reg_write_enb_d;
    reg     [NB_INDEX_ALU_OP-1:0]   alu_op_d;
    reg                             alu_src_d;
    reg     [2:0]                   exe_jump_type_d;
    reg                             mem_write_enabled_d;
    reg                             mem_read_enabled_d;
    reg    [1:0]                    mem_branch_d;
    reg    [1:0]                    mem_byte_enb_d;
    reg    [1:0]                    mem_ext_sig_d;
    reg                             write_back_ctl_d;   
    reg    [25:0]                   addr_index;
    
    wire                            reg_write_dest_w;
    wire                            reg_write_enb_w;
    wire    [NB_INDEX_ALU_OP-1:0]   alu_op_w;
    wire                            alu_src_w;
    wire    [2:0]                   exe_jump_type_w;
    wire                            mem_write_enabled_w;
    wire                            mem_read_enabled_w;
    wire    [1:0]                   mem_branch_w;
    wire    [1:0]                   mem_byte_enb_w;
    wire    [1:0]                   mem_ext_sig_w;
    wire                            write_back_ctl_w;

    
    always @(posedge i_clk)
    begin     
        if (i_reset)
        begin
            reg_write_dest_d <= 0;
            reg_write_enb_d <= 0;
            alu_op_d <= 0;
            alu_src_d <= 0;
            exe_jump_type_d <= 0;
            mem_write_enabled_d <= 0;
            mem_read_enabled_d <= 0;
            mem_branch_d <= 0;            
            mem_byte_enb_d <= 0;
            mem_ext_sig_d <= 0;
            write_back_ctl_d <= 0;

        end 
        else if(i_mips_enable && i_nop)
        begin
            reg_write_dest_d <= 0;
            reg_write_enb_d <= 0;
            alu_op_d <= 0;
            alu_src_d <= 0;
            exe_jump_type_d <= 0;
            mem_write_enabled_d <= 0;
            mem_read_enabled_d <= 0;
            mem_branch_d <= 0;            
            mem_byte_enb_d <= 0;
            mem_ext_sig_d <= 0;
            write_back_ctl_d <= 0;
        end
        else if (i_mips_enable)
        begin
            reg_write_dest_d <= reg_write_dest_w;
            reg_write_enb_d <= reg_write_enb_w;
            alu_op_d <= alu_op_w;
            alu_src_d <= alu_src_w;
            exe_jump_type_d <= exe_jump_type_w;
            mem_write_enabled_d <= mem_write_enabled_w;
            mem_read_enabled_d <= mem_read_enabled_w;
            mem_branch_d <= mem_branch_w;            
            mem_byte_enb_d <= mem_byte_enb_w;
            mem_ext_sig_d <= mem_ext_sig_w;
            write_back_ctl_d <= write_back_ctl_w;
        end
    end 
                 
    
    //Decode instruction
    assign read_rs_addr = i_instruction [25:21];
    assign read_rt_addr = i_instruction [20:16];
    assign write_reg_addr = i_instruction [15:11];
    assign  o_reg_write_dest = reg_write_dest_d;   
    assign  o_reg_write_enb = reg_write_enb_d;
    assign  o_alu_op = alu_op_d;
    assign  o_alu_src = alu_src_d;
    assign  o_exe_jump_type = exe_jump_type_d;
    assign  o_mem_write_enabled = mem_write_enabled_d;
    assign  o_mem_read_enabled = mem_read_enabled_d;
    assign  o_mem_branch = mem_branch_d;
    assign  o_mem_byte_enb = mem_byte_enb_d;
    assign  o_mem_ext_sig = mem_ext_sig_d;
    assign  o_write_back_ctl = write_back_ctl_d;
           
    always @(posedge i_clk)
    begin
        if (i_reset)
            pc_plus4 <= 0; 
        else if(i_mips_enable && i_nop)
        begin
            pc_plus4 <= 0; 
        end 
        else if (i_mips_enable)
            pc_plus4 <= i_pc_plus4;
    end
    assign o_pc_plus4 = pc_plus4;
    
    always @(posedge i_clk)
    begin
        if (i_reset)
        begin
            rs_data <= {32{1'b0}};
            rt_data <= {32{1'b0}};
        end  
        else if (i_mips_enable && i_nop)
        begin
            rs_data <= {32{1'b0}};
            rt_data <= {32{1'b0}};
        end    
        else if (i_mips_enable)
        begin 
            rs_data <=  register_file_rs_data;                            
            rt_data <= register_file_rt_data;
        end
    end       
    
    assign o_rs_data = rs_data;
    assign o_rt_data = rt_data;
    
    always @(posedge i_clk)
    begin
        if (i_reset)
            sign_extend <= 0; 
        else if (i_mips_enable && i_nop)
            sign_extend <= 0;
        else if (i_mips_enable)
            sign_extend <= {{16{i_instruction[15]}},{i_instruction[15:0]}};
    end
    
    assign o_sign_extend = sign_extend;
    
    reg [NB_INDEX_REG-1:0] read_rs_addr_d;
    reg [NB_INDEX_REG-1:0] read_rt_addr_d;
    reg [NB_INDEX_REG-1:0] write_reg_addr_d;
    always @(posedge i_clk)
    begin
        if (i_reset)
        begin
            read_rs_addr_d <= 0;
            read_rt_addr_d <= 0;
            write_reg_addr_d <= 0;
            addr_index <= 0;
        end  
        else if (i_mips_enable && i_nop)
        begin
            read_rs_addr_d <= 0;
            read_rt_addr_d <= 0;
            write_reg_addr_d <= 0;
            addr_index <= 0;
        end
        else if (i_mips_enable)
        begin
            read_rs_addr_d <= read_rs_addr;        
            read_rt_addr_d <= read_rt_addr;
            write_reg_addr_d <= write_reg_addr;
            addr_index <= i_instruction [25:0];
        end
    end
    
    assign o_read_rs_addr = read_rs_addr_d;
    assign o_read_rt_addr = read_rt_addr_d;
    assign o_write_reg_addr = write_reg_addr_d;  
    assign  o_addr_index = addr_index;
  
    
     //instancia register_file        
    register_file registers(
        .i_clk                  (i_clk),
        .i_reset                (i_reset),
        .i_write_enable         (i_mips_enable & i_reg_write_enb),
        .i_read_rs_addr         (read_rs_addr),
        .i_read_rt_addr         (read_rt_addr),
        .i_write_reg_adrr       (i_reg_write_dest),
        .i_write_reg_data       (i_write_reg_data),
            
        .o_rs_data              (register_file_rs_data),
        .o_rt_data              (register_file_rt_data)  
    );
    
    main_control_unit control_unit(
        .i_instruction_op(i_instruction[31:26]),   
        .i_instruction_func(i_instruction[5:0]), 
        //ID
        .o_id_reg_dest(reg_write_dest_w),      //al mux de ex
        .o_id_reg_write(reg_write_enb_w),      //reg_write_enb --> wb 
        //EX  
        .o_ex_alu_op(alu_op_w),
        .o_ex_alu_src(alu_src_w),
        .o_exe_jump_type(exe_jump_type_w),
        //MEM
        .o_mem_write_enabled(mem_write_enabled_w),
        .o_mem_read_enabled(mem_read_enabled_w),
        .o_mem_branch(mem_branch_w),       //es PCSrc 
        .o_mem_byte_enb(mem_byte_enb_w),
        .o_mem_ext_sig(mem_ext_sig_w),
        //WB
        .o_write_back_ctl(write_back_ctl_w)                 //memToReg
    );
   
endmodule
