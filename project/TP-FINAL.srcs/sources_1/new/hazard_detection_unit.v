`timescale 1ns / 1ps

module hazard_detection_unit#(
    parameter N_REG = 32,
    parameter NB_INDEX_REG = $clog2(N_REG),
    parameter NB_RW_MEM = 2
    
)(
    input wire [NB_INDEX_REG-1:0] i_id_rt_index,
    input wire [NB_INDEX_REG-1:0] i_if_rs_index,
    input wire [NB_INDEX_REG-1:0] i_if_rt_index,
    input wire [NB_RW_MEM-1:0]    i_id_rw_mem,          //conectamos desde afuera primero R y segundo W
    
    output wire o_id_nop,
    output wire o_if_stall
    );
    
    assign o_id_nop = ((i_id_rw_mem == 2'b10) && 
                    ((i_id_rt_index == i_if_rs_index) || 
                    (i_id_rt_index == i_if_rt_index)))? 1'b1 : 1'b0;
                    
    assign o_if_stall = ((i_id_rw_mem == 2'b10) && 
                    ((i_id_rt_index == i_if_rs_index) || 
                    (i_id_rt_index == i_if_rt_index)))? 1'b1 : 1'b0;
       
endmodule
