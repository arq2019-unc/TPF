`timescale 1ns / 100ps
 
module EX#(
    parameter NB_DATA       =  32,
    parameter N_REG         =  32,
    parameter NB_INDEX_REG  =  $clog2(N_REG),
    parameter NB_INDEX_ALU_OP  =  3,
    parameter NB_FORWARD_SEL = 2

)(
    input wire i_clk,	
	input wire i_reset,
	input wire i_mips_enable,
	input wire ALUSrc,
	input wire [2:0] i_exe_jump_type,
	input wire [25:0] i_addr_index,
	input wire [NB_INDEX_ALU_OP-1:0] ALUop,
    input wire [NB_DATA-1:0] i_rs_data,
    input wire [NB_DATA-1:0] i_rt_data,
    input wire [NB_DATA-1:0] i_sign_extend,
    input wire [NB_DATA-1:0] i_pc_plus4,    
    input wire i_regdest,                       //viene de la unidad de control  y se queda aca. no sigue    
    input wire [NB_INDEX_REG-1:0] i_read_rt_addr,
    input wire [NB_INDEX_REG-1:0] i_write_reg_addr, 
    input wire [NB_DATA-1:0] i_fwd_wrb_data,
    
    input wire i_reg_write_enb,               //sigue a la siguiente etapa
    input wire i_mem_write_enabled,
    input wire i_mem_read_enabled, 
    input wire [1:0] i_mem_branch,
    input wire [1:0] i_mem_byte_enb,
    input wire [1:0] i_mem_ext_sig,
    input wire i_write_back_ctl,
    input [NB_FORWARD_SEL-1:0] i_fwd_rs_select,
    input [NB_FORWARD_SEL-1:0] i_fwd_rt_select,
    
    output [NB_DATA-1:0] o_rt_data,
    //output wire o_zero,
    output wire [NB_DATA-1:0] o_add_pc_plus4_shift_left2,
    output wire [NB_INDEX_REG-1:0] o_reg_write_dest,
    output wire [NB_DATA-1:0] o_alu_result,
    output wire o_reg_write_enb,               //sigue hasta WB etapa a etapa     
    
    output wire o_mem_write_enabled,
    output wire o_mem_read_enabled, 
    //output wire o_mem_branch, no iria mas
    output wire o_branch_taken,
    output wire [1:0] o_mem_byte_enb,
    output wire [1:0] o_mem_ext_sig,
    output wire o_write_back_ctl,
    
    output wire o_jump,
	output wire [NB_DATA-1:0] o_jump_addr
);

    //local
    wire   [NB_DATA- 1 : 0]   alu_result;
    wire   [NB_DATA- 1 : 0]   sec_alu;
    wire   [5:0]              shift_left2;
    reg    [NB_DATA- 1 : 0]   result;   
    wire   [5:0]              i_alu_control_input;
    wire   [NB_INDEX_REG-1:0] reg_write_dest;
    reg    [NB_INDEX_REG-1:0] reg_write_dest_d;
    reg    [NB_DATA-1:0]      add_pc_plus4_shift_left2;
    //reg                       zero;
    wire                      alu_zero_ex;
    reg    [NB_DATA-1:0]      rt_data;
    wire   [NB_DATA-1:0]      fw_rs_data;
    wire   [NB_DATA-1:0]      fw_rt_data;
    reg                       mem_write_enabled;
    reg                       mem_read_enabled; 
    //reg                       mem_branch;
    reg     [1:0]             mem_byte_enb;
    reg     [1:0]             mem_ext_sig;
    reg                       write_back_ctl;
    reg                       reg_write_enb;
    
    assign sec_alu = (ALUSrc)?  i_sign_extend : fw_rt_data; 
    assign o_rt_data = rt_data;
    assign shift_left2 = i_sign_extend [5:0];
    //assign o_add_pc_plus4_shift_left2 = add_pc_plus4_shift_left2;
    assign o_add_pc_plus4_shift_left2 = $signed(shift_left2) + $signed(i_pc_plus4);   //ver si hace falta $signed
    assign o_reg_write_dest = reg_write_dest_d;
    assign o_mem_write_enabled = mem_write_enabled;
    assign o_mem_read_enabled = mem_read_enabled; 
    //assign o_mem_branch = mem_branch;
    assign o_mem_byte_enb = mem_byte_enb;
    assign o_mem_ext_sig = mem_ext_sig;
    assign o_write_back_ctl = write_back_ctl;
    assign o_reg_write_enb = reg_write_enb;
                   
    assign o_branch_taken = ((i_mem_branch == 2'b01) & alu_zero_ex) ? 1'b1 : ((i_mem_branch == 2'b10) & (~alu_zero_ex)) ? 1'b1 : 1'b0; 
      
    assign fw_rs_data  = (i_fwd_rs_select == 2'b01 )
                  ? result
                  : (i_fwd_rs_select == 2'b10 )
                    ? i_fwd_wrb_data
                    :  i_rs_data;
        
    assign fw_rt_data  = (i_fwd_rt_select == 2'b01 )
                  ? result
                  : (i_fwd_rt_select == 2'b10 )
                    ? i_fwd_wrb_data
                    :  i_rt_data;
       
    always @(posedge i_clk)
    begin
        if(i_reset)
            mem_byte_enb <= 0;
        else if (i_mips_enable)
            mem_byte_enb <= i_mem_byte_enb;  
    end
    
    always @(posedge i_clk)
    begin
        if(i_reset)
            mem_ext_sig <= 0;
        else if (i_mips_enable)
            mem_ext_sig <= i_mem_ext_sig;  
    end
        
    always @(posedge i_clk)
    begin
        if(i_reset)
            reg_write_enb <= 0;
        else if (i_mips_enable)
            reg_write_enb <= i_reg_write_enb;  
    end
    
    always @(posedge i_clk)
    begin
        if(i_reset)
            mem_write_enabled <= 0;
        else if (i_mips_enable)
            mem_write_enabled <= i_mem_write_enabled;  
    end
    
    always @(posedge i_clk)
    begin
        if(i_reset)
            mem_read_enabled <= 0;
        else if (i_mips_enable)
            mem_read_enabled <= i_mem_read_enabled;  
    end
    
//    always @(posedge i_clk)
//    begin
//        if(i_reset)
//            mem_branch <= 0;
//        else
//            mem_branch <= i_mem_branch;  
//    end
    
    always @(posedge i_clk)
    begin
        if(i_reset)
            write_back_ctl <= 0;
        else if (i_mips_enable)
            write_back_ctl <= i_write_back_ctl;  
    end
    
    always @(posedge i_clk)
    begin
        if(i_reset)
            rt_data <= 0;
        else if (i_mips_enable)
            rt_data <= fw_rt_data;  
    end
    
//    always @(posedge i_clk)
//    begin
//        if(i_reset)
//            add_pc_plus4_shift_left2 <= 0;
//        else
//            add_pc_plus4_shift_left2 <= shift_left2 + i_pc_plus4;  
//    end
    
    always @(posedge i_clk)
    begin
        if(i_reset)
            reg_write_dest_d <= 0;
        else if (i_mips_enable) 
        begin
            if(i_exe_jump_type == 3'b010) //JAL
                reg_write_dest_d <= 'd31;
            else if(i_exe_jump_type == 3'b010) //JALR
                reg_write_dest_d <= i_write_reg_addr;
            else
                reg_write_dest_d <= (i_regdest) ? i_write_reg_addr : i_read_rt_addr;   //se puede hacer asi?
        end            
    end
    
    wire select_alu_result;
    assign select_alu_result =  ((i_exe_jump_type == 3'b010) //JAL
                              || (i_exe_jump_type == 3'b100) //JALR
                            )  ? 1'b0 : 1'b1;
    
    always @(posedge i_clk)
    begin
        if(i_reset)
            result <= 'd0;        
        else if (i_mips_enable)
            result <= (select_alu_result)?alu_result : i_pc_plus4 + 1'b1;
    end
    
//    always @(posedge i_clk)
//    begin
//        if(i_reset)
//            zero <= 0;        
//        else
//            zero <= alu_zero_ex;
//    end
    
    ALU alu(
        .i_rs_data(fw_rs_data),
        .i_rt_data(sec_alu),
        .i_shamt(i_sign_extend[10:6]),      //verificar no estoy seguro que este bien, sino hay q pasar la instruccion por todas las etapas
        .i_func(i_alu_control_input),
        .o_result(alu_result),
        .o_zero(alu_zero_ex)     
    );
    
    alu_control_unit alu_control(
        .i_func_field(i_sign_extend[5:0]),
        .i_alu_op(ALUop),	
	    .o_alu_control(i_alu_control_input)
    );

    assign o_alu_result = result;
    //assign o_zero = zero;
    
    
    //PC JUMP
    assign o_jump_addr = ((i_exe_jump_type == 3'b001)      //J
                           || (i_exe_jump_type == 3'b010) // JAL
                         )? {2'b00, i_pc_plus4[NB_DATA - 1 -: 4],i_addr_index} : fw_rs_data; 

    assign o_jump    =  (i_exe_jump_type != 3'b000) ? 1'b1 : 1'b0;   //sin jump?
    
endmodule
