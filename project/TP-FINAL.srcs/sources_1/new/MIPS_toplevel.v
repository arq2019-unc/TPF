`timescale 1ns / 1ps

module MIPS_toplevel#(
    parameter NB_DATA       =  32,
    parameter N_REG         =  32,
    parameter NB_INDEX_REG  =  $clog2(N_REG),
    parameter NB_INDEX_ALU_OP  =  3,
    parameter NB_FORWARD_SEL = 2,
    parameter ROM_FILE = ""

)(
    input wire i_clk,	
	input wire i_reset,
	input wire i_mips_enable,
	input wire i_extern_halt,	
	input wire instruction_write_enb,
    input wire [NB_DATA-1:0] instruction_addr,
    input wire [NB_DATA-1:0] instruction_data
    );
            
    
    
    wire instruction_write_enb_w;
    wire [NB_DATA-1:0] instruction;
    wire [NB_DATA-1:0] o_rs;
    wire [NB_DATA-1:0] o_rt;
    wire [NB_DATA-1:0] o_sign_extend;
    wire [NB_INDEX_ALU_OP-1:0]   o_alu_op;
    wire [NB_DATA-1:0] id_pcplus4_ex;
    wire o_alu_src;
    wire [2:0] id_exe_jump_type_ex;
    wire id_mem_write_enabled_ex;
    wire id_mem_read_enabled_ex;
    wire [1:0] id_mem_branch_ex;
    wire id_write_back_ctl_ex; 
    wire [NB_DATA-1:0] ex_add_pc_plus4_shift_left2_if;
    wire mem_pc_src_if;
    wire [NB_DATA-1:0] if_pc_plus4_id;
    wire [NB_INDEX_REG-1:0] wb_reg_write_dest_id;
    wire [NB_DATA-1:0] wb_reg_write_data_id;
    wire [NB_INDEX_REG-1:0] id_read_rs_addr_fw;
    wire [NB_INDEX_REG-1:0] id_read_rt_addr_ex;
    wire [NB_INDEX_REG-1:0] id_write_reg_adrr_ex;
    wire id_reg_write_dest_ex;
    wire [NB_DATA-1:0] ex_write_data_mem;
    wire ex_zero_to_branch_mem;
    wire [NB_DATA-1:0] ex_add_pc_plus4_shift_left2_mem;
    wire [NB_INDEX_REG-1:0] ex_reg_write_dest_mem;
    wire [NB_DATA-1:0] ex_alu_result_mem;
    wire ex_mem_branch_mem;
    wire ex_mem_write_enable_mem;
    wire ex_mem_read_enable_mem; 
    wire ex_write_back_ctl_mem;
    wire [NB_DATA-1:0] mem_data_adrr_wb;
    wire [NB_DATA-1:0] mem_read_data_wb;
    wire [NB_INDEX_REG-1:0]  mem_reg_write_dest_wb;
    wire mem_write_back_ctl_wb;
    wire wb_reg_write_enb_id;
    wire id_reg_write_enb_ex;
    wire ex_reg_write_enb_mem;
    wire mem_reg_write_enb_wb;
    wire [NB_FORWARD_SEL-1:0] fw_rs_select_ex;
    wire [NB_FORWARD_SEL-1:0] fw_rt_select_ex;
    wire [1:0] id_mem_byte_enb_ex;
    wire [1:0] id_mem_ext_sig_ex;
    wire [1:0] ex_mem_byte_enb_mem;
    wire [1:0] ex_mem_ext_sig_mem;
    wire hazard_nop_id;
    wire hazard_stall_if;
    wire ex_jump_taken_if;
    wire [NB_DATA-1:0] ex_jump_addr_if;
    wire [25:0] id_addr_index_ex;    
    
//    always @(*)
//    begin    
//        instruction_write_enb = 0;
//        instruction_addr = 0;
//        instruction_data = 0;
//    end
     
    assign instruction_write_enb_w = instruction_write_enb;
    
    //instancia IF            
    IF#(
        .ROM_FILE(ROM_FILE)
    ) mips_if(
       .i_clk(i_clk),	
       .i_reset(i_reset),
       .i_mips_enable(i_mips_enable),
       .i_instruction_write_enb(instruction_write_enb_w),
       .i_instruction_addr(instruction_addr),
       .i_instruction_data(instruction_data),
       .i_add_pc_plus4_shift_left2(ex_add_pc_plus4_shift_left2_if),
       .i_pc_src(mem_pc_src_if),  
       .i_stall(hazard_stall_if),
       .i_jump(ex_jump_taken_if),
	   .i_jump_addr(ex_jump_addr_if), //32b
	   .i_extern_halt(i_extern_halt),
         
       .o_IF(instruction),
       .o_pc_plus4(if_pc_plus4_id)

    );
    //instancia ID
    ID mips_id(
        .i_clk(i_clk),	
        .i_reset(i_reset),
        .i_mips_enable(i_mips_enable),
        .i_instruction(instruction),
        .i_reg_write_dest(wb_reg_write_dest_id),      
        .i_write_reg_data(wb_reg_write_data_id), 
        .i_pc_plus4(if_pc_plus4_id),
        .i_reg_write_enb(wb_reg_write_enb_id),
        .i_nop(hazard_nop_id | mem_pc_src_if),        
        
        .o_rs_data(o_rs),
        .o_rt_data(o_rt),
        .o_read_rs_addr(id_read_rs_addr_fw),
        .o_read_rt_addr(id_read_rt_addr_ex),
        .o_write_reg_addr(id_write_reg_adrr_ex), 
        .o_sign_extend(o_sign_extend),
        .o_pc_plus4(id_pcplus4_ex),
        
        .o_reg_write_enb(id_reg_write_enb_ex),                       //va de etapa a etapa hasta WB
        .o_alu_op(o_alu_op),
        .o_alu_src(o_alu_src),
        .o_exe_jump_type(id_exe_jump_type_ex),
        .o_addr_index(id_addr_index_ex),
        .o_mem_write_enabled(id_mem_write_enabled_ex),
        .o_mem_read_enabled(id_mem_read_enabled_ex), 
        .o_mem_branch(id_mem_branch_ex),
        .o_mem_byte_enb(id_mem_byte_enb_ex),
        .o_mem_ext_sig(id_mem_ext_sig_ex),
        .o_write_back_ctl(id_write_back_ctl_ex),
        .o_reg_write_dest(id_reg_write_dest_ex)                      //va solo hasta EX
   );
    
    //instancia EX    
    EX mips_ex(
        .i_clk(i_clk),	
	    .i_reset(i_reset),
	    .i_mips_enable(i_mips_enable),
	    .ALUSrc(o_alu_src),
	    .i_exe_jump_type(id_exe_jump_type_ex),
	    .i_addr_index(id_addr_index_ex),  
	    .ALUop(o_alu_op),
        .i_rs_data(o_rs),
        .i_rt_data(o_rt),
        .i_sign_extend(o_sign_extend),
        .i_pc_plus4(id_pcplus4_ex),    
        .i_regdest(id_reg_write_dest_ex),                       //viene de la unidad de control y entra hasta aca, no sigue
        .i_read_rt_addr(id_read_rt_addr_ex),
        .i_write_reg_addr(id_write_reg_adrr_ex),
        .i_fwd_wrb_data(wb_reg_write_data_id),                  //------> esta bien?
        .i_reg_write_enb(id_reg_write_enb_ex),                  //sigue de etapa a etapa hasta WB
 
    
        .i_mem_write_enabled(id_mem_write_enabled_ex),
        .i_mem_read_enabled(id_mem_read_enabled_ex), 
        .i_mem_branch(id_mem_branch_ex),
        .i_mem_byte_enb(id_mem_byte_enb_ex),
        .i_mem_ext_sig(id_mem_ext_sig_ex),
        .i_write_back_ctl(id_write_back_ctl_ex),
        
        .i_fwd_rs_select(fw_rs_select_ex),
        .i_fwd_rt_select(fw_rt_select_ex),
        
        .o_rt_data(ex_write_data_mem),
        //.o_zero(ex_zero_to_branch_mem),
        .o_add_pc_plus4_shift_left2(ex_add_pc_plus4_shift_left2_if),
        .o_reg_write_dest(ex_reg_write_dest_mem),
        .o_alu_result(ex_alu_result_mem),
        .o_reg_write_enb(ex_reg_write_enb_mem),

        .o_mem_write_enabled(ex_mem_write_enable_mem),
        .o_mem_read_enabled(ex_mem_read_enable_mem), 
        //.o_mem_branch(ex_mem_branch_mem),
        .o_branch_taken(mem_pc_src_if),
        .o_mem_byte_enb(ex_mem_byte_enb_mem),
        .o_mem_ext_sig(ex_mem_ext_sig_mem),
        .o_write_back_ctl(ex_write_back_ctl_mem),
        
        .o_jump(ex_jump_taken_if),
	    .o_jump_addr(ex_jump_addr_if)

    );
    
    //instancia MEM
    MEM mips_mem(
        .i_clk(i_clk),	
	    .i_reset(i_reset),
	    .i_mips_enable(i_mips_enable),
	    .i_data_write_enb(ex_mem_write_enable_mem),
        .i_data_read_enb(ex_mem_read_enable_mem),              // La lectura de una direccion no valida puede causar problemas
        .i_data_addr(ex_alu_result_mem),
        .i_write_data(ex_write_data_mem),
        //.i_add_pc_plus4_shift_left2(ex_add_pc_plus4_shift_left2_mem),
        .i_reg_write_dest(ex_reg_write_dest_mem),
        //.i_zero(ex_zero_to_branch_mem),
        //.i_mem_branch(ex_mem_branch_mem),
        .i_mem_byte_enb(ex_mem_byte_enb_mem),
        .i_mem_ext_sig(ex_mem_ext_sig_mem),
        .i_write_back_ctl(ex_write_back_ctl_mem),        
        .i_reg_write_enb(ex_reg_write_enb_mem),

    
        .o_data_addr(mem_data_adrr_wb),
        .o_read_data(mem_read_data_wb),
        //.o_add_pc_plus4_shift_left2(mem_add_pc_plus4_shift_left2_if),
        .o_reg_write_dest(mem_reg_write_dest_wb), 
        //.o_branch_taken(mem_pc_src_if),
        .o_write_back_ctl(mem_write_back_ctl_wb),
        .o_reg_write_enb(mem_reg_write_enb_wb)

    );
    
    WB mips_wb(        
        .i_mem_read_data(mem_read_data_wb),
        .i_alu_result(mem_data_adrr_wb),        
        .i_mem_to_reg(mem_write_back_ctl_wb),
        .i_reg_write_dest(mem_reg_write_dest_wb),
        .i_reg_write_enb(mem_reg_write_enb_wb),

   
        .o_reg_write_data(wb_reg_write_data_id),
        .o_reg_write_addr(wb_reg_write_dest_id),
        .o_reg_write_enb(wb_reg_write_enb_id)
    );
    
    forwarding_unit MIPS_fw_u(
        //from ID
        .i_rs_addr(id_read_rs_addr_fw),      //ID->FW
        .i_rt_addr(id_read_rt_addr_ex),     //ID->EX
        //from EX  
        .i_ex_reg_dest(ex_reg_write_dest_mem), //EX->mem
        .i_ex_wb_ctl(ex_reg_write_enb_mem),   //EX->mem   ex_write_back_ctl_mem,ex_reg_write_enb_mem
        //from MEM
        .i_mem_reg_dest(mem_reg_write_dest_wb), //MEM->WB
        .i_mem_wb_ctl(mem_reg_write_enb_wb),     //MEM->WB  mem_write_back_ctl_wb,wb_reg_write_enb_id,mem_reg_write_enb_wb
        //to EX
        .o_fwd_rs_select(fw_rs_select_ex),    
        .o_fwd_rt_select(fw_rt_select_ex)
    );        
    
    hazard_detection_unit MIPS_hazard_u(
        .i_id_rt_index(id_read_rt_addr_ex),
        .i_if_rs_index(instruction[25:21]),
        .i_if_rt_index(instruction[20:16]),
        .i_id_rw_mem({id_mem_read_enabled_ex,id_mem_write_enabled_ex}),          //conectamos desde afuera primero R y segundo W        
        
        .o_id_nop(hazard_nop_id),
        .o_if_stall(hazard_stall_if)
    );
    
endmodule