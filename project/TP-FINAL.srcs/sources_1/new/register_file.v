`timescale 1ns / 1ps

module register_file(
    input wire  i_clk,
    input wire  i_reset,
    input wire  i_write_enable,
    input wire [4:0] i_read_rs_addr,
    input wire [4:0] i_read_rt_addr,
    input wire [4:0] i_write_reg_adrr,
    input wire [31:0] i_write_reg_data,
        
    output  wire [31:0] o_rs_data,
    output  wire [31:0] o_rt_data    
    );
    
    reg [31:0] registers [31:0];
    integer i;
    
    always @ (negedge i_clk)
    begin
        if (i_reset)
        begin
            for (i = 0 ; i < 32 ; i = i + 1)                
                registers[i] <= i;  //registers[i] <= {32{1'b0}};
        end
        else if (i_write_enable)
            registers[i_write_reg_adrr] <= i_write_reg_data;
    end
    
    assign o_rs_data = registers[i_read_rs_addr];
    assign o_rt_data = registers[i_read_rt_addr];
    
endmodule
