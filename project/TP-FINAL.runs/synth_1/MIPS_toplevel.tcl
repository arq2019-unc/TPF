# 
# Synthesis run script generated by Vivado
# 

set TIME_start [clock seconds] 
proc create_report { reportName command } {
  set status "."
  append status $reportName ".fail"
  if { [file exists $status] } {
    eval file delete [glob $status]
  }
  send_msg_id runtcl-4 info "Executing : $command"
  set retval [eval catch { $command } msg]
  if { $retval != 0 } {
    set fp [open $status w]
    close $fp
    send_msg_id runtcl-5 warning "$msg"
  }
}
create_project -in_memory -part xc7a100tcsg324-3

set_param project.singleFileAddWarning.threshold 0
set_param project.compositeFile.enableAutoGeneration 0
set_param synth.vivado.isSynthRun true
set_property webtalk.parent_dir {C:/Users/Usuario/Desktop/Arquitectura de Computadoras/TPFinal Mips/TPF/project/TP-FINAL.cache/wt} [current_project]
set_property parent.project_path {C:/Users/Usuario/Desktop/Arquitectura de Computadoras/TPFinal Mips/TPF/project/TP-FINAL.xpr} [current_project]
set_property default_lib xil_defaultlib [current_project]
set_property target_language Verilog [current_project]
set_property ip_output_repo {c:/Users/Usuario/Desktop/Arquitectura de Computadoras/TPFinal Mips/TPF/project/TP-FINAL.cache/ip} [current_project]
set_property ip_cache_permissions {read write} [current_project]
read_mem {
  {C:/Users/Usuario/Desktop/Arquitectura de Computadoras/TPFinal Mips/TPF/project/TP-FINAL.srcs/sources_1/new/rom_test.mem}
  {C:/Users/Usuario/Desktop/Arquitectura de Computadoras/TPFinal Mips/TPF/project/TP-FINAL.srcs/sources_1/new/data_test.mem}
  {C:/Users/Usuario/Desktop/Arquitectura de Computadoras/TPFinal Mips/TPF/project/TP-FINAL.srcs/sources_1/new/test_BNE_BEQ.mem}
  {C:/Users/Usuario/Desktop/Arquitectura de Computadoras/TPFinal Mips/TPF/project/TP-FINAL.srcs/sources_1/new/sim_arithmetic_logic.mem}
  {C:/Users/Usuario/Desktop/Arquitectura de Computadoras/TPFinal Mips/TPF/project/TP-FINAL.srcs/sources_1/new/sim_bitwise.mem}
  {C:/Users/Usuario/Desktop/Arquitectura de Computadoras/TPFinal Mips/TPF/project/TP-FINAL.srcs/sources_1/new/sim_branchs_jumps.mem}
  {C:/Users/Usuario/Desktop/Arquitectura de Computadoras/TPFinal Mips/TPF/project/TP-FINAL.srcs/sources_1/new/sim_load_store.mem}
  {C:/Users/Usuario/Desktop/Arquitectura de Computadoras/TPFinal Mips/TPF/project/TP-FINAL.srcs/sources_1/new/sim_loadUse_useStore.mem}
  {C:/Users/Usuario/Desktop/Arquitectura de Computadoras/TPFinal Mips/TPF/project/TP-FINAL.srcs/sources_1/new/sim_fibonacci.mem}
}
read_verilog -library xil_defaultlib {
  {C:/Users/Usuario/Desktop/Arquitectura de Computadoras/TPFinal Mips/TPF/project/TP-FINAL.srcs/sources_1/new/EX.v}
  {C:/Users/Usuario/Desktop/Arquitectura de Computadoras/TPFinal Mips/TPF/project/TP-FINAL.srcs/sources_1/new/ID.v}
  {C:/Users/Usuario/Desktop/Arquitectura de Computadoras/TPFinal Mips/TPF/project/TP-FINAL.srcs/sources_1/new/IF.v}
  {C:/Users/Usuario/Desktop/Arquitectura de Computadoras/TPFinal Mips/TPF/project/TP-FINAL.srcs/sources_1/new/MEM.v}
  {C:/Users/Usuario/Desktop/Arquitectura de Computadoras/TPFinal Mips/TPF/project/TP-FINAL.srcs/sources_1/new/ROM.v}
  {C:/Users/Usuario/Desktop/Arquitectura de Computadoras/TPFinal Mips/TPF/project/TP-FINAL.srcs/sources_1/new/WB.v}
  {C:/Users/Usuario/Desktop/Arquitectura de Computadoras/TPFinal Mips/TPF/project/TP-FINAL.srcs/sources_1/new/alu.v}
  {C:/Users/Usuario/Desktop/Arquitectura de Computadoras/TPFinal Mips/TPF/project/TP-FINAL.srcs/sources_1/new/alu_control_unit.v}
  {C:/Users/Usuario/Desktop/Arquitectura de Computadoras/TPFinal Mips/TPF/project/TP-FINAL.srcs/sources_1/new/forwarding_unit.v}
  {C:/Users/Usuario/Desktop/Arquitectura de Computadoras/TPFinal Mips/TPF/project/TP-FINAL.srcs/sources_1/new/hazard_detection_unit.v}
  {C:/Users/Usuario/Desktop/Arquitectura de Computadoras/TPFinal Mips/TPF/project/TP-FINAL.srcs/sources_1/new/main_control_unit.v}
  {C:/Users/Usuario/Desktop/Arquitectura de Computadoras/TPFinal Mips/TPF/project/TP-FINAL.srcs/sources_1/new/memory_data.v}
  {C:/Users/Usuario/Desktop/Arquitectura de Computadoras/TPFinal Mips/TPF/project/TP-FINAL.srcs/sources_1/new/register_file.v}
  {C:/Users/Usuario/Desktop/Arquitectura de Computadoras/TPFinal Mips/TPF/project/TP-FINAL.srcs/sources_1/new/MIPS_toplevel.v}
}
# Mark all dcp files as not used in implementation to prevent them from being
# stitched into the results of this synthesis run. Any black boxes in the
# design are intentionally left as such for best results. Dcp files will be
# stitched into the design at a later time, either when this synthesis run is
# opened, or when it is stitched into a dependent implementation run.
foreach dcp [get_files -quiet -all -filter file_type=="Design\ Checkpoint"] {
  set_property used_in_implementation false $dcp
}
set_param ips.enableIPCacheLiteLoad 1
close [open __synthesis_is_running__ w]

synth_design -top MIPS_toplevel -part xc7a100tcsg324-3 -flatten_hierarchy none -directive RuntimeOptimized -fsm_extraction off


# disable binary constraint mode for synth run checkpoints
set_param constraints.enableBinaryConstraints false
write_checkpoint -force -noxdef MIPS_toplevel.dcp
create_report "synth_1_synth_report_utilization_0" "report_utilization -file MIPS_toplevel_utilization_synth.rpt -pb MIPS_toplevel_utilization_synth.pb"
file delete __synthesis_is_running__
close [open __synthesis_is_complete__ w]
