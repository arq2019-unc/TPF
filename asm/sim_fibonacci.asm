start:  
        ADDI R4, R0, 0    
        ADDI R1, R0, 0
        ADDI R3, R3, 7
        ADDI R1, R1, 1
fiboFunc:
        ADDU R2, R1, R0
        ADDI R0, R1, 0
        ADDI R1, R2, 0
        ADDI R4, R4, 1
        BNE  R4, R3, fiboFunc
        ADDI R0, R2, 0 // R0=RESULTADO DE FIBONACCI
        LUI  R1, 0
        SW   R0, 0(R1) // memory[0]=RESULTADO DE FIBONACCI
        halt
