main:
	ADDI R4, R0, 0
	ADDI R2, R0, 4      	//R2=4 | si R2=3-> toma otro camino
	ADDI R3, R0, 4 			//R3=4
	BEQ  R2, R3, Label1 	//toma el branch a Label1
	ADDI R4, R0, 4 			//no ejecuta
	J Label3 				//no ejecuta
Label1:
	ADDI R4, R0, 3			//R4=3    |ejecuta si toma J Label3 
	BNE  R3, R4, Label2 	//toma el branch a Label2
	ADDI R4, R0, 2          //no ejecuta
	ADDI R4, R0, 1          //no ejecuta

Label2: 
	ADDI R5, R0, 16			//R5=16
	JR   R5 				//jump a 16 -> halt
	ADDI R4, R0, 2          //R4=2 
	ADDI R4, R0, 0          //no ejecuta

Label3:
	JAL final 				//no ejecuta

final:
	ADDI R6, R0, 42 		//no ejecuta
	halt 					//PC frenado
	