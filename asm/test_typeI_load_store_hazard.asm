start:
	LB 		R15, 0(R8)		//R15=byte(memory[2]) 8° byte de la memoria
	ADDU 	R31, R0, R15	//R31=R15=byte(memory[2])
	LH 		R16, 0(R12)		//R16=half(memory[3]) 12° byte de la memoria
	LHU		R17, 0(R16)		//R17=half(memory[9]) 36° byte de la memoria
	ADDU    R18, R0, R1 	//R18=1
	ADDU	R19, R1, R2 	//R19=3
	ADDU	R20, R0, R19 	//R20=3
	halt
	