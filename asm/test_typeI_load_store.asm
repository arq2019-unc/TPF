start:
	LB 		R15, 0(R8)		//R15=memory[2] con ext sig
	ADDU 	R31, R25, R25
	ADDU 	R30, R25, R25
	ADDU 	R29, R25, R25
	ADDU 	R28, R25, R25
	LH 		R16, 0(R12)		//R16=memory[3] con ext sig
	ADDU 	R31, R25, R25
	ADDU 	R30, R25, R25
	ADDU 	R29, R25, R25
	ADDU 	R28, R25, R25
	LW   	R17, 1(R7)		//R17=memory[2] con ext sig
	ADDU 	R31, R25, R25
	ADDU 	R30, R25, R25
	ADDU 	R29, R25, R25
	ADDU 	R28, R25, R25
	LBU		R18, 0(R12)		//R18=memory[2] sin ext sig
	ADDU 	R31, R25, R25
	ADDU 	R30, R25, R25
	ADDU 	R29, R25, R25
	ADDU 	R28, R25, R25
	LHU		R19, 1(R11)		//R19=memory[3] sin ext sig
	ADDU 	R31, R25, R25
	ADDU 	R30, R25, R25
	ADDU 	R29, R25, R25
	ADDU 	R28, R25, R25
	LWU		R20, 1(R0)		//R20=memory[2] sin ext sig
	ADDI 	R31, R25, 100000
	ADDU 	R30, R25, R25
	ADDU 	R29, R25, R25
	ADDU 	R28, R25, R25
	SB		R31, 16(R0)	//memory[7] = R31[7:0]
	ADDU 	R31, R31, R0
	ADDU 	R30, R25, R25
	ADDU 	R29, R25, R25
	ADDU 	R28, R25, R25
	SH		R31, 28(R0)	//memory[8] = R31[15:0]
	ADDU 	R31, R31, R0
	ADDU 	R30, R25, R25
	ADDU 	R29, R25, R25
	ADDU 	R28, R25, R25
	SW		R31, 32(R0)	//memory[9] = R31[31:0]
	halt
	